﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
public class ChangeText : MonoBehaviour
{

    public GameObject changingText;
    // Start is called before the first frame update
    
    public void TextChange()
    {
        var json = System.IO.File.ReadAllText("Assets/Resources/Commands.json");
        string jsonText = (json);
        changingText.GetComponent<Text>().text = jsonText;
        
    }
}
